package main

import (
	"github.com/prometheus/client_golang/prometheus"
)

const namespace = "alertmanager_hubbub"

var (
	alertsCount = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "alerts_created_total",
			Help:      "The number of Alertmanager Hubbub created alerts.",
		},
	)
	errorsCount = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "errors_total",
			Help:      "The number of Alertmanager request errors.",
		},
	)
)

func init() {
	prometheus.MustRegister(
		alertsCount,
		errorsCount,
	)
}
