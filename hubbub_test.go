package main

import (
	"fmt"
	"net/http"
	"testing"
	"time"

	"github.com/go-openapi/strfmt"
	"github.com/prometheus/alertmanager/api/v2/client"
	"github.com/prometheus/alertmanager/api/v2/client/silence"
	"github.com/prometheus/alertmanager/api/v2/models"
)

func TestNewHubbub(t *testing.T) {
	config := &Configuration{
		AlertmanagerURL: "http://localhost:9093",
	}
	_, err := NewHubbub(config)
	if err != nil {
		t.Error(err)
	}
	config.AlertmanagerURL = "%zzzzz"
	_, err = NewHubbub(config)
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
}

func flushSilences(t *testing.T, amclient *client.AlertmanagerAPI) {
	resp, err := amclient.Silence.GetSilences(nil)
	if err != nil {
		t.Error(err)
		return
	}
	for _, s := range resp.Payload {
		if time.Time(*s.EndsAt).Before(time.Now()) {
			continue
		}
		deleteParams := silence.NewDeleteSilenceParams().WithSilenceID(strfmt.UUID(*s.ID))
		_, err := amclient.Silence.DeleteSilence(deleteParams)
		if err != nil {
			t.Log(err)
		}
	}
}

func TestGetCurrentState(t *testing.T) {
	_, err := http.Get(fmt.Sprintf("%s/-/healthy", defConfig.AlertmanagerURL))
	if err != nil {
		t.Skip("Run Alertmanager before tests")
	}
	h, err := NewHubbub(defConfig)
	if err != nil {
		t.Error(err)
	}

	flushSilences(t, h.amClient)
	time.Sleep(100 * time.Millisecond)

	if err := h.run(); err != nil {
		t.Error(err)
	}

	now := time.Now()
	startsAt := strfmt.DateTime(now)
	endsAt := strfmt.DateTime(now.Add(5 * time.Minute))
	cm := "foobar"
	labelNameA := "alertname"
	labelValueA := "bar"
	isRegexA := false
	labelNameB := "faa"
	labelValueB := "faa"
	isRegexB := true
	labelNameC := "severity"
	labelValueC := "critical"
	isRegexC := false
	labelNameD := "foo"
	labelValueD := "bar"
	isRegexD := false
	ps := &models.PostableSilence{
		Silence: models.Silence{
			StartsAt:  &startsAt,
			EndsAt:    &endsAt,
			Comment:   &cm,
			CreatedBy: &cm,
			Matchers: models.Matchers{
				&models.Matcher{
					Name:    &labelNameA,
					Value:   &labelValueA,
					IsRegex: &isRegexA,
				},
				&models.Matcher{
					Name:    &labelNameB,
					Value:   &labelValueB,
					IsRegex: &isRegexB,
				},
				&models.Matcher{
					Name:    &labelNameC,
					Value:   &labelValueC,
					IsRegex: &isRegexC,
				},
				&models.Matcher{
					Name:    &labelNameD,
					Value:   &labelValueD,
					IsRegex: &isRegexD,
				},
			},
		},
	}
	silenceParams := silence.NewPostSilencesParams()
	silenceParams.Silence = ps
	_, err = h.amClient.Silence.PostSilences(silenceParams)
	if err != nil {
		t.Error(err)
	}

	time.Sleep(100 * time.Millisecond)

	if err := h.run(); err != nil {
		t.Error(err)
	}

	flushSilences(t, h.amClient)
	time.Sleep(100 * time.Millisecond)

	if err := h.run(); err != nil {
		t.Error(err)
	}
}

func TestIsAllowedToNotify(t *testing.T) {
	filters, err := ParseSilenceFilter("group=project-infra|proj-infra|proj-build-infra|project-build-infra,foo=bar")
	if err != nil {
		t.Fatal(err)
	}
	c := &Configuration{
		SilenceFilter: filters,
	}
	h, err := NewHubbub(c)
	if err != nil {
		t.Fatal(err)
	}
	tests := []struct {
		name, value   string
		isRegex, want bool
	}{
		{
			name:    "alertname",
			value:   "bar",
			isRegex: false,
			want:    false,
		},
		{
			name:    "group",
			value:   "foobar",
			isRegex: false,
			want:    false,
		},
		{
			name:    "group",
			value:   "project-infra",
			isRegex: false,
			want:    true,
		},
		{
			name:    "bar",
			value:   "foo",
			isRegex: false,
			want:    false,
		},
		{
			name:    "foo",
			value:   "bar",
			isRegex: false,
			want:    true,
		},
	}
	instanceName := "instance"
	instanceValue := "localhost"
	instanceRegExp := false
	gs := &models.GettableSilence{}
	for _, test := range tests {
		gs.Silence.Matchers = models.Matchers{
			&models.Matcher{
				Name:    &instanceName,
				Value:   &instanceValue,
				IsRegex: &instanceRegExp,
			},
			&models.Matcher{
				Name:    &test.name,
				Value:   &test.value,
				IsRegex: &test.isRegex,
			},
		}
		if h.IsAllowedToNotify(gs) != test.want {
			t.Errorf("IsFiltered(%s: %s) != %v", test.name, test.value, test.want)
		}
	}
}
