package main

import (
	"regexp"
	"time"
)

const (
	defaultAlertTemplate = "ID: <{{.GeneratorURL}}|{{.Silence.ID}}>\nCreated by: {{.Silence.CreatedBy}}\nDuration: {{.Duration}}\nComment: {{.Silence.Comment}}\nMatchers: `{{.Matchers}}`"
)

var (
	defConfig = &Configuration{
		AlertmanagerURL:             "http://localhost:9093",
		AlertGeneratorURL:           "http://localhost:9093",
		SkipRegExpMatches:           true,
		RepeatInterval:              5 * time.Minute,
		WebListenAddress:            ":8080",
		WebTelemetryPath:            "/metrics",
		AllowedUnavailabilityPeriod: 30 * time.Minute,
		AlertTTL:                    time.Minute,
		AlertTemplate:               defaultAlertTemplate,
		AlertSeverity:               "info",
	}
)

type Configuration struct {
	AlertmanagerURL             string
	AlertGeneratorURL           string
	SkipRegExpMatches           bool
	RepeatInterval              time.Duration
	WebListenAddress            string
	WebTelemetryPath            string
	AllowedUnavailabilityPeriod time.Duration
	AlertTTL                    time.Duration
	AlertTemplate               string
	AlertSeverity               string
	SilenceFilter               map[string]*regexp.Regexp
}
