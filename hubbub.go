package main

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/dustin/go-humanize"
	"github.com/go-openapi/strfmt"
	"github.com/prometheus/alertmanager/api/v2/client"
	"github.com/prometheus/alertmanager/api/v2/client/alert"
	"github.com/prometheus/alertmanager/api/v2/client/silence"
	"github.com/prometheus/alertmanager/api/v2/models"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
)

const (
	GeneratorURLFormat = "%s/#/silences/%s"

	AlertnameLabel = "alertname"
	SeverityLabel  = "severity"

	AddedSilenceAlertname   = "SilenceAdded"
	ExpiredSilenceAlertname = "SilenceExpired"
)

type Hubbub struct {
	config    *Configuration
	amClient  *client.AlertmanagerAPI
	fetchTime time.Time
	active    []*models.GettableSilence
	expired   []*models.GettableSilence
}

func NewHubbub(config *Configuration) (*Hubbub, error) {
	h := &Hubbub{
		config: config,
	}
	alertmanagerURL, err := url.Parse(config.AlertmanagerURL)
	if err != nil {
		return nil, err
	}
	h.amClient = NewAlertmanagerClient(alertmanagerURL)
	return h, nil
}

func (h *Hubbub) GetCurrentState() ([]*models.GettableSilence, []*models.GettableSilence, error) {
	var (
		active  []*models.GettableSilence
		expired []*models.GettableSilence
	)
	silenceParams := silence.NewGetSilencesParams().WithContext(context.Background())
	silencesOk, err := h.amClient.Silence.GetSilences(silenceParams)
	if err != nil {
		return nil, nil, err
	}
	for _, s := range silencesOk.Payload {
		if time.Time(*s.EndsAt).Before(time.Now()) {
			expired = append(expired, s)
		}
		if time.Time(*s.EndsAt).After(time.Now()) {
			active = append(active, s)
		}
	}
	return active, expired, nil
}

func (h *Hubbub) updateState(active, expired []*models.GettableSilence) {
	h.active = active
	h.expired = expired
}

func (h *Hubbub) run() error {
	logrus.Debug("Requesting new state...")

	currentActive, currentExpired, err := h.GetCurrentState()
	if err != nil {
		errorsCount.Inc()
		return err
	}

	if h.fetchTime.IsZero() {
		h.fetchTime = time.Now()
		h.updateState(currentActive, currentExpired)
		return nil
	}

	if time.Since(h.fetchTime) > h.config.AllowedUnavailabilityPeriod {
		logrus.Warn("Flushing state after period on unavailability of Alertmanager")
		h.fetchTime = time.Now()
		h.updateState(currentActive, currentExpired)
		return nil
	}

	activeAdded := getAddedSilences(h.active, currentActive)
	if len(activeAdded) > 0 {
		logrus.Infof("Added silences: %d", len(activeAdded))
		h.Notify(activeAdded, false)
	}

	expiredAdded := getAddedSilences(h.expired, currentExpired)
	if len(expiredAdded) > 0 {
		logrus.Infof("Expired silences: %d", len(expiredAdded))
		h.Notify(expiredAdded, true)
	}

	h.updateState(currentActive, currentExpired)

	return nil
}

func (h *Hubbub) Notify(silences []*models.GettableSilence, expired bool) {
	for _, s := range silences {
		if !h.IsAllowedToNotify(s) {
			continue
		}

		alertsCount.Inc()

		err := h.notify(s, expired)
		if err != nil {
			alertsCount.Inc()
			logrus.Error(err)
		}
	}
}

func (h *Hubbub) IsAllowedToNotify(silence *models.GettableSilence) bool {
	if len(h.config.SilenceFilter) == 0 {
		return true
	}
	for _, matcher := range silence.Matchers {
		for name, re := range h.config.SilenceFilter {
			if *matcher.Name != name {
				continue
			}
			if !re.MatchString(*matcher.Value) {
				continue
			}
			return true
		}
	}
	return false
}

func (h *Hubbub) notify(silence *models.GettableSilence, expired bool) error {
	alertName := AddedSilenceAlertname
	if expired {
		alertName = ExpiredSilenceAlertname
	}
	labels := models.LabelSet{
		AlertnameLabel: alertName,
		SeverityLabel:  h.config.AlertSeverity,
	}

	var labelsOriginal []string
	for _, matcher := range silence.Matchers {
		labelsOriginal = append(labelsOriginal, fmt.Sprintf("%s=%s", *matcher.Name, *matcher.Value))
		if h.config.SkipRegExpMatches && *matcher.IsRegex {
			continue
		}
		if *matcher.Name == AlertnameLabel {
			continue
		}
		if *matcher.Name == SeverityLabel {
			continue
		}
		labels[*matcher.Name] = *matcher.Value
	}

	logrus.WithField("id", *silence.ID).WithField("expired", expired).
		Debugf("Matchers: %v", labelsOriginal)

	generatorURL := fmt.Sprintf(GeneratorURLFormat, h.config.AlertGeneratorURL, *silence.ID)
	silenceDuration := humanize.RelTime(time.Time(*silence.StartsAt), time.Time(*silence.EndsAt), "", "")
	data := map[string]interface{}{
		"GeneratorURL": generatorURL,
		"Silence":      silence,
		"Matchers":     strings.Join(labelsOriginal, ", "),
		"Duration":     silenceDuration,
	}
	description, err := GenerateTemplate(h.config.AlertTemplate, data)
	if err != nil {
		return err
	}
	logrus.WithField("id", *silence.ID).Debugf("Description: %s", description)
	annotations := models.LabelSet{
		"description": description,
	}

	startsAt := time.Now()
	endsAt := time.Now().Add(h.config.AlertTTL)

	pa := &models.PostableAlert{
		Alert: models.Alert{
			GeneratorURL: strfmt.URI(generatorURL),
			Labels:       labels,
		},
		Annotations: annotations,
		StartsAt:    strfmt.DateTime(startsAt),
		EndsAt:      strfmt.DateTime(endsAt),
	}

	alertParams := alert.NewPostAlertsParams().WithContext(context.Background()).
		WithAlerts(models.PostableAlerts{pa})
	_, err = h.amClient.Alert.PostAlerts(alertParams)
	return err
}

func (h *Hubbub) serveMetrics() {
	logrus.Printf("Providing metrics at %s%s", h.config.WebListenAddress, h.config.WebTelemetryPath)
	http.Handle(h.config.WebTelemetryPath, promhttp.Handler())
	logrus.Fatal(http.ListenAndServe(h.config.WebListenAddress, nil))
}

func (h *Hubbub) Run() error {
	go h.serveMetrics()
	for {
		err := h.run()
		if err != nil {
			logrus.Error(err)
		} else {
			h.fetchTime = time.Now()
		}
		time.Sleep(h.config.RepeatInterval)
	}
}

func getAddedSilences(oldState, newState []*models.GettableSilence) []*models.GettableSilence {
	var result []*models.GettableSilence
	for _, newSilence := range newState {
		match := false
		for _, oldSilence := range oldState {
			if *oldSilence.ID == *newSilence.ID {
				match = true
			}
		}
		if !match {
			result = append(result, newSilence)
		}
	}
	return result
}
