FROM golang:1.21-alpine3.17 AS builder
WORKDIR /go/src/gitlab.com/leominov/alertmanager-hubbub
COPY . .
RUN go build .

FROM alpine:3.17
COPY --from=builder /go/src/gitlab.com/leominov/alertmanager-hubbub/alertmanager-hubbub /
ENTRYPOINT [ "/alertmanager-hubbub" ]
