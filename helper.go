package main

import (
	"bytes"
	"fmt"
	"net/url"
	"path"
	"regexp"
	"strings"
	"text/template"

	clientruntime "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
	"github.com/prometheus/alertmanager/api/v2/client"
)

const (
	defaultAmHost      = "localhost"
	defaultAmPort      = "9093"
	defaultAmAPIv2path = "/api/v2"
)

func NewAlertmanagerClient(amURL *url.URL) *client.AlertmanagerAPI {
	address := defaultAmHost + ":" + defaultAmPort
	schemes := []string{"http"}

	if amURL.Host != "" {
		address = amURL.Host // URL documents host as host or host:port
	}
	if amURL.Scheme != "" {
		schemes = []string{amURL.Scheme}
	}

	cr := clientruntime.New(address, path.Join(amURL.Path, defaultAmAPIv2path), schemes)

	if amURL.User != nil {
		password, _ := amURL.User.Password()
		cr.DefaultAuthentication = clientruntime.BasicAuth(amURL.User.Username(), password)
	}

	return client.New(cr, strfmt.Default)
}

func GenerateTemplate(templ string, data interface{}) (string, error) {
	var templateEng *template.Template
	buf := bytes.NewBufferString("")
	templateEng = template.New("text")
	if messageTempl, err := templateEng.Parse(templ); err != nil {
		return "", fmt.Errorf("failed to parse template: %v", err)
	} else if err := messageTempl.Execute(buf, data); err != nil {
		return "", fmt.Errorf("failed to execute template: %v", err)
	}
	return buf.String(), nil
}

func ParseSilenceFilter(filter string) (map[string]*regexp.Regexp, error) {
	result := make(map[string]*regexp.Regexp)
	kvs := strings.Split(filter, ",")
	for _, kv := range kvs {
		kv = strings.TrimSpace(kv)
		data := strings.Split(kv, "=")
		if len(data) != 2 {
			continue
		}
		re, err := regexp.Compile(data[1])
		if err != nil {
			return nil, err
		}
		result[data[0]] = re
	}
	return result, nil
}
