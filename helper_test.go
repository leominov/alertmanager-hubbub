package main

import (
	"net/url"
	"testing"
)

func TestParseSilenceFilter(t *testing.T) {
	regs, err := ParseSilenceFilter("foo=bar")
	if err != nil {
		t.Error(err)
	}
	if len(regs) != 1 {
		t.Errorf("Must be 1, but got %d", len(regs))
	}
	regs, err = ParseSilenceFilter("foo=bar,doo=faa,faa")
	if err != nil {
		t.Error(err)
	}
	if len(regs) != 2 {
		t.Errorf("Must be 2, but got %d", len(regs))
	}
	_, err = ParseSilenceFilter(`foo=([\xx`)
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
}

func TestGenerateTemplate(t *testing.T) {
	r, err := GenerateTemplate("{{.Item}}", map[string]string{"Item": "ABC"})
	if err != nil {
		t.Error(err)
	}
	if r != "ABC" {
		t.Errorf("Must be ABC, but got %s", r)
	}
	_, err = GenerateTemplate("{{.Item", map[string]string{"Item": "ABC"})
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
}

func TestNewAlertmanagerClient(t *testing.T) {
	addrs := []string{
		"foobar:1080",
		"http://foobar",
		"http://user:pass@foobar",
	}
	for _, addr := range addrs {
		u, err := url.Parse(addr)
		if err != nil {
			t.Error()
		}
		cli := NewAlertmanagerClient(u)
		if cli == nil {
			t.Error("Client nil returned")
		}
	}
}
