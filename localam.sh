#!/usr/bin/env bash
set -euo pipefail

echo "Starting Alertmanager..."
docker rm -f dev-am &> /dev/null || :
docker run --cap-add=IPC_LOCK -d --name=dev-am -p 9093:9093 prom/alertmanager:v0.27.0 &> /dev/null
echo "...done. Run \"docker rm -f dev-am\" to clean up the container."
echo
