package main

import (
	"flag"
	"os"

	"github.com/sirupsen/logrus"
)

var (
	alertGeneratorURL           = flag.String("alert.generator-url", defConfig.AlertGeneratorURL, "Alertmanager URL to use in alerts")
	alertmanagerURL             = flag.String("alertmanager.url", defConfig.AlertmanagerURL, "Alertmanager to talk to")
	alertSeverity               = flag.String("alert.severity", defConfig.AlertSeverity, "Severity label value for alert")
	alertTemplate               = flag.String("alert.template", defConfig.AlertTemplate, "Template for alert description annotation")
	alertTemplateFile           = flag.String("alert.template-file", "", "Path to file with template for alert description annotation")
	alertTTL                    = flag.Duration("alert.ttl", defConfig.AlertTTL, "Time to life for notification alerts")
	allowedUnavailabilityPeriod = flag.Duration("alertmanager.allowed-unavailability-period", defConfig.AllowedUnavailabilityPeriod, "Flush state after period of Alertmanager unavailability")
	logLevel                    = flag.String("log-level", "info", "Level of logging")
	repeatInterval              = flag.Duration("repeat-interval", defConfig.RepeatInterval, "Repeat interval for Alertmanager requests")
	silenceFilter               = flag.String("silence.filter", "", "Label filter for given silences (In K=V format separated by comma)")
	skipRegExpMatchers          = flag.Bool("alert.skip-regexp-matchers", defConfig.SkipRegExpMatches, "Skip silence labels with RegExp values")
	webListenAddress            = flag.String("web.listen-address", defConfig.WebListenAddress, "Address to listen on for telemetry")
	webMetricPath               = flag.String("web.telemetry-path", defConfig.WebTelemetryPath, "Path under which to expose metrics")
)

func main() {
	flag.Parse()

	// Output logs in JSON format
	logrus.SetFormatter(&logrus.JSONFormatter{})

	if level, err := logrus.ParseLevel(*logLevel); err != nil {
		logrus.Warn(err)
	} else {
		logrus.SetLevel(level)
	}

	silenceFilter, err := ParseSilenceFilter(*silenceFilter)
	if err != nil {
		logrus.Fatal(err)
	}

	c := &Configuration{
		AlertmanagerURL:             *alertmanagerURL,
		AlertGeneratorURL:           *alertGeneratorURL,
		SkipRegExpMatches:           *skipRegExpMatchers,
		RepeatInterval:              *repeatInterval,
		AllowedUnavailabilityPeriod: *allowedUnavailabilityPeriod,
		AlertTTL:                    *alertTTL,
		AlertTemplate:               *alertTemplate,
		AlertSeverity:               *alertSeverity,
		SilenceFilter:               silenceFilter,
		WebListenAddress:            *webListenAddress,
		WebTelemetryPath:            *webMetricPath,
	}

	if len(*alertTemplateFile) > 0 {
		logrus.Infof("Loading alert template from %s file...", *alertTemplateFile)
		body, err := os.ReadFile(*alertTemplateFile)
		if err != nil {
			logrus.Fatal(err)
		}
		c.AlertTemplate = string(body)
	}

	h, err := NewHubbub(c)
	if err != nil {
		logrus.Fatal(err)
	}
	err = h.Run()
	if err != nil {
		logrus.Fatal(err)
	}
}
